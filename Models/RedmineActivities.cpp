#include "RedmineActivities.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

/* -------------------------------------------
 *  Model zu Darstellung einer Aktivität
 *  zu einer erfassten Zeit
 * ----------------------------------------- */

/* -------------------------------------------
 *  Konstruktor
 * ----------------------------------------- */
RedmineActivities::RedmineActivities() {
    name = "";
}

/* -------------------------------------------
 *  Destruktor
 * ----------------------------------------- */
RedmineActivities::~RedmineActivities() {

}

/* -------------------------------------------
 *  Parst einen JSON String zu einer Liste
 *  von Aktivitäten
 * ----------------------------------------- */
QList<RedmineActivities*> RedmineActivities::createActivitiesFromJson(QString jsonString) {
   //TODO
    QList<RedmineActivities*> activities = QList<RedmineActivities*>();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["time_entry_activities"].toArray();

    try{
        foreach (const QJsonValue & value, jsonArray)
        {
            QJsonObject obj = value.toObject();
            RedmineActivities *activity = new RedmineActivities();
            activity->id =obj["id"].toInt();
            activity->name =obj["name"].toString();
            activities.append(activity);
        }
    }
    catch(std::exception & e){
    }
    return activities;
}
