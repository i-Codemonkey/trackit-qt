#ifndef REDMINETIMEENTRY_H
#define REDMINETIMEENTRY_H

#include <QtCore/qdatetime.h>
#include <QtCore/QAssociativeIterable>
#include "Models/RedmineIssue.h"
#include "Models/RedmineProject.h"
#include "Models/RedmineActivities.h"
#include <QUuid>

class RedmineTimeEntry
{
public:
    RedmineTimeEntry();

public:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    QUuid uid;                      /*!< Die Datenbank ID */
    QString comments;               /*!< Der Kommentar zum aktuellen Zeiteintrag */
    QDate spentOn;                  /*!< Das Datum an dem die Zeit erfasst wurde */
    QTime *Time;                    /*!< Die Zeit der erfassung */
    double spendTime;               /*!< Die erfasste Zeit in Stunden */
    RedmineActivities *activity;    /*!< Die Aktivität für die Zeit erfasst wurde */
    RedmineIssue *issue;            /*!< Das Ticket für das Zeit aufgewand wurde */
    RedmineProject *project;        /*!< Das Projekt dem das Ticket angehöhrt */
    QString redmineServerUrl;       /*!< Die Url auf dem sich Projekt und Ticket sich befinden und der Aufwand später gepublished werden soll */
};
#endif // REDMINETIMEENTRY_H
