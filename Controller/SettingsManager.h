#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H
#include <QtCore>
#include "Models/RedmineAccount.h"

class SettingsManager
{
public:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Statische Methoden (Singleton)
     * ------------------------------------------------------------------------------------------------------------------------------- */
    static SettingsManager* instance()
    {
        static QMutex mutex;
        if (!_instance)
        {
            mutex.lock();

            if (!_instance)
                _instance = new SettingsManager;
                _instance->loadSettings();
            mutex.unlock();
        }
        return _instance;
    }

    static void drop()
    {
        static QMutex mutex;
        mutex.lock();
        delete _instance;
        _instance = 0;
        mutex.unlock();
    }

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Public Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    QString redmineServerAddress;       /*!< Die Url zum Redmine Server */
    QString redmineUser;                /*!< Der Username mit dem der Benutzer sich am Redmine anmeldet  */
    QString redminePassword;            /*!< Der Passwort mit dem der Benutzer sich am Redmine anmeldet */
    QString redmineAPIKey;              /*!< Der API Key mit dem der Benutzer sich am Redmine anmeldet */
    bool redmineUseApiKeyAuthMethod;    /*!< Gibt an welche Authentifizierungsmethode genutzt wird API Key oder Benutzername/Passwort */

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Public Methoden
     * ------------------------------------------------------------------------------------------------------------------------------- */
    //! Erstellt aus den einzelnen Authentifizierungsdaten ein Objekt vom Typ RedmineAccount
    /*!
      \return Gibt die Authentifizierungsdaten als RedmineAccount zurück
    */
    RedmineAccount* redmineAccountData();

    //! Läd alle Einstellungen
    /*!
    */
    void loadSettings();

    //! Speichert alle Einstellungen auf dem lokalen Dateisystem
    /*!
    */
    void writeSettings();

    //! Prüft ob die geladenen Einstellung gültig sind um den Server zu kontaktieren
    /*!
     \return Gibt true zurück wenn die Daten gültig sind sonst false
    */
    bool isValidConfigAvailable();

private:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     *  Konstruktor
     * ------------------------------------------------------------------------------------------------------------------------------- */
    SettingsManager() {}
    SettingsManager(const SettingsManager &); // verdeckt den Konstruktor
    SettingsManager& operator=(const SettingsManager &); // versteckt den zuweisungsoperator

    static SettingsManager* _instance;
};

#endif // SETTINGSMANAGER_H
