#include "stdio.h"
#include "KeyAuthenticator.h"
#include "PasswordAuthenticator.h"
#include "RedmineClient.h"
#include "Models/RedmineProject.h"
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkConfigurationManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QSslError>
#include <QMessageBox>
#include <QUrl>
#include <QUrlQuery>



/* ---------------------------------------------------------------------------------------------------------------------------------
 * Constructor
 * ------------------------------------------------------------------------------------------------------------------------------- */

RedmineClient::RedmineClient(QUrl url, QString apiKey, QObject* parent) : QObject(parent), _url(url) {
    _authenticator = new KeyAuthenticator(apiKey.toUtf8());
	init();
}

RedmineClient::RedmineClient(QUrl url, QString login, QString password) :  _url(url) {
	_authenticator = new PasswordAuthenticator(login, password);
	init();
}

RedmineClient::RedmineClient(RedmineAccount *account){
    _url = account->hostUrl;
    _authenticator = new PasswordAuthenticator(account->username, account->password);
    init();
}


/* ---------------------------------------------------------------------------------------------------------------------------------
 * Destructor
 * ------------------------------------------------------------------------------------------------------------------------------- */

RedmineClient::~RedmineClient() {
    delete _networkManager;
	delete _authenticator;
}

/* ---------------------------------------------------------------------------------------------------------------------------------
 * Request Methods
 * ------------------------------------------------------------------------------------------------------------------------------- */

void RedmineClient::init() {
    _networkManager = new QNetworkAccessManager(this);
    QDebug(new QString("init slot"));
    _userAgent = "redmine-qt";
}

/* ---------------------------------------------------------------------------------------------------------------------------------
 * Request Methods
 * ------------------------------------------------------------------------------------------------------------------------------- */

QNetworkRequest RedmineClient::prepareRequest(QUrl url){

    QNetworkRequest request;
    request.setRawHeader("User-Agent",_userAgent);
    request.setRawHeader("X-Custom-User-Agent", _userAgent);
    request.setRawHeader("Content-Type", "application/json; charset=utf-8");

    _authenticator->addAuthentication(&request);

    request.setUrl(url);

    return request;
}

void RedmineClient::getProjects(){
    QString projectUrl = _url.toString() + QString("/projects.json?limit=100");
    _currentReply = _networkManager->get(this->prepareRequest(projectUrl));

    if(!_currentReply->isFinished()){
        connect(_currentReply, SIGNAL(finished()),this, SLOT(finishedProjectsRequest()));
    }
}

void RedmineClient::getIssues(QString projectId){
    QString issueUrl = _url.toString() + QString("/projects/") + projectId + QString("/issues.json");
    _currentReply = _networkManager->get(this->prepareRequest(issueUrl));
    if(!_currentReply->isFinished()){
        connect(_currentReply, SIGNAL(finished()),this, SLOT(finishedIssuesRequest()));
    }
}

void RedmineClient::getActivities(){
    QString activitiesUrl = _url.toString() + QString("/enumerations/time_entry_activities.json");
    _currentReply = _networkManager->get(this->prepareRequest(activitiesUrl));
    if(!_currentReply->isFinished()){
        connect(_currentReply, SIGNAL(finished()),this, SLOT(finishedActivitiesRequest()));
    }
}

/* ---------------------------------------------------------------------------------------------------------------------------------
 * Response Slots
 * ------------------------------------------------------------------------------------------------------------------------------- */

void RedmineClient::finishedProjectsRequest(){
    // no error received?
    if (_currentReply->error() == QNetworkReply::NoError)
    {
        QString jsonString = (QString) _currentReply->readAll();

        QList<RedmineProject*> projects = RedmineProject::createProjectsFromJson(jsonString);
        emit fetchRedmineProjectsCompleted(projects);
    }
    // Some http error received
    else {
        handleError();
    }
    disconnect(_currentReply, SIGNAL(finished()),this,SLOT(finishedProjectsRequest()));
}

void RedmineClient::finishedActivitiesRequest(){

    // no error received?
    if (_currentReply->error() == QNetworkReply::NoError)
    {
        QString jsonString = (QString) _currentReply->readAll();
        QList<RedmineActivities*> activities = RedmineActivities::createActivitiesFromJson(jsonString);
        emit fetchTimeEntryActivitiesCompleted(activities);
    }
    // Some http error received
    else {
        handleError();
    }
    disconnect(_currentReply, SIGNAL(finished()),this,SLOT(finishedActivitiesRequest()));
}

void RedmineClient::finishedIssuesRequest(){
    // no error received?
    if (_currentReply->error() == QNetworkReply::NoError)
    {
        QString jsonString = (QString) _currentReply->readAll();
        QMap<int,RedmineIssue*> issues = RedmineIssue::createIssuesFromJson(jsonString);
        emit fetchProjectIssuesCompleted(issues);
    }
    // Some http error received
    else {
        handleError();
    }
    disconnect(_currentReply, SIGNAL(finished()),this,SLOT(finishedIssuesRequest()));
}

void RedmineClient::finishedTimeEntryRequest(){
    if (_currentReply->error() == QNetworkReply::NoError){
        emit postTimeEntryCompleted(true);
    }else{
        emit postTimeEntryCompleted(false);
    }
}

void RedmineClient::postTimeEntry(RedmineTimeEntry *entry){
    QString timeEntryPostUrl = "";

    if(entry->redmineServerUrl != "") {
        timeEntryPostUrl = entry->redmineServerUrl + QString("/time_entries.json");
    } else {
        timeEntryPostUrl = _url.toString() + QString("/time_entries.json");
    }

    QString params = "";
    params.append("?time_entry[issue_id]=" + QString::number(entry->issue->id));
    params.append("&time_entry[spent_on]=" + entry->spentOn.toString("yyyy-MM-dd"));
    params.append("&time_entry[hours]=" + QString::number(entry->spendTime,'f', 2));
    params.append("&time_entry[activity_id]=" + QString::number(entry->activity->id));
    params.append("&time_entry[comments]=" + entry->comments);

    timeEntryPostUrl.append(params);

    QNetworkRequest request = prepareRequest(timeEntryPostUrl);

    QByteArray postData;
    _currentReply = _networkManager->post(request,postData);
    if(!_currentReply->isFinished()){
        connect(_currentReply, SIGNAL(finished()),this, SLOT(finishedTimeEntryRequest()));
    }
}

void RedmineClient::handleError() {
    QString message = "";
    if (!QNetworkConfigurationManager().isOnline()) {
        message = "No Internet Connection.";
    } else {
        switch (_currentReply->error()) {
        case QNetworkReply::HostNotFoundError:
            message = "Host Not Found!\nPlease check your server settings.";
            break;
        case QNetworkReply::ContentOperationNotPermittedError:
            message = "Access Denied\nDetails: The selected project can not be accessed.";
            break;
        default:
            message = "Unknown Error\n" + _currentReply->errorString();
            break;
        }
    }
    emit apiErrorOccurs(message);
}



