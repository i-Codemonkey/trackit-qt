#ifndef I_AUTHENTICATOR_HPP
#define I_AUTHENTICATOR_HPP

class QNetworkRequest;

/* ---------------------------------------------------------------------------------------------------------------------------------
 *  Authenticator interface.
 *  Händelt die Authentifizierung für einen Netzwerkanfrage
 * ------------------------------------------------------------------------------------------------------------------------------- */
class IAuthenticator {
public:
	virtual ~IAuthenticator() {}
	virtual void addAuthentication(QNetworkRequest* request) = 0;
};

#endif // I_AUTHENTICATOR_HPP
