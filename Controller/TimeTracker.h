#ifndef TIMETRACKER_H
#define TIMETRACKER_H

#include <QtCore>
#include <QTime>
#include <QTimer>

class TimeTracker : public QObject {

    Q_OBJECT

signals:
    //! Wird jedes mal aufgerufen wenn der Timer sich aktualisiert. Kann genutzt werden um UI zu aktualisieren
    /*!
    */
    void update(QString timeString);

public:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Konstruktor & Destruktor
     * ------------------------------------------------------------------------------------------------------------------------------- */
    TimeTracker();
    ~TimeTracker();

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Public Mehtoden
     * ------------------------------------------------------------------------------------------------------------------------------- */

    //! Startet den Timer
    /*!
    */
    void startTime();

    //! Setzt den Timer und alle Werte auf die Ausgangswerte zurück
    /*!
    */
    void resetTime();

    //! Hält den Timer an
    /*!
    */
    void stopTime();

    //! Gibt das Aktuelle Time Objekt zurück
    /*!
    */
    QTime* currentTime();

    //! Gibt die Aufgewendeten Stunden als Gleitkommazahl zurück
    /*!
    */
    double getHours();

public slots:
    void tick();
private:
    int temp_seconds;           /*!< temporär werden die Sekunden mitgezählt */
    QTime *time;                /*!< In diesem Objekt wird die Zeit mit getrackt */
    QTimer *timer;              /*!< Das Timer Objekt aktualsiert jede Sekunde  */
    double spendTime;           /*!< Die aufgewendete Zeit in Stunden */
};

#endif // TIMETRACKER_H
