#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "stdio.h"
#include "iostream"
#include "Controller/SettingsManager.h"
#include "ViewController/dialogsettings.h"
#include "Models/RedmineAccount.h"
#include "Constants.h"
#include <QMessageBox>
#include <QListWidgetItem>
#include <QApplication>
#include <QtGui>

/*-------------------------------------------------------------------------------
 * Konstruktor & Destruktor
--------------------------------------------------------------------------------*/

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    this->trackerController = new TimeTracker();
    ui->setupUi(this);
    this->setupCustomUI();
    this->connectSlots();
    this->loadUnpublishedEntries();
    if(!SettingsManager::instance()->isValidConfigAvailable()){
        DialogSettings *settings = new DialogSettings(this);
        settings->exec();
    }else{
        reloadRedmineData();
    }
}

MainWindow::~MainWindow() {
    delete ui;
    delete lblTimer;
    delete progress;
    delete trackerController;
    delete redmineManager;
}

/*-------------------------------------------------------------------------------
 * UI Slots
--------------------------------------------------------------------------------*/

void MainWindow::btnSettingsClicked() {
    // öffnet einen Einstellungsdialog
    DialogSettings *settings = new DialogSettings(this);
    settings->open();
}

void MainWindow::btnRefreshClicked(){
    // läd die daten vom server neu
    reloadRedmineData();
}

void MainWindow::btnRecordTimeClicked(bool toggled){
    // Wenn der Button getoggled ist dann wird der Timer gestartet
    if(toggled){
        trackerController->resetTime();
        trackerController->startTime();
        // Control wird angepasst
        ui->actionRecordTime->setText("Stop");
        // Controls werden deaktiviert für die zeit der aufnahme
        ui->cmbIssues->setEnabled(false);
        ui->cmbProjects->setEnabled(false);
        // Objekt für Zeiterfassung wird mit den ersten daten wie projekt, ticket und serverurl initialisiert
        this->currentTimeRecord = RedmineTimeEntry();
        currentTimeRecord.project = ui->cmbProjects->itemData(ui->cmbProjects->currentIndex()).value<RedmineProject*>();
        currentTimeRecord.issue = ui->cmbIssues->itemData(ui->cmbIssues->currentIndex()).value<RedmineIssue*>();
        currentTimeRecord.redmineServerUrl = SettingsManager::instance()->redmineServerAddress;
    }else{
        // Aufnahme wird beendet
        // setzen der aufgenommenen Zeit im Objekt
        currentTimeRecord.spendTime = trackerController->getHours();
        // Stoppen des timers
        trackerController->stopTime();
        // Anzeigen der Messagebox ob der Zeiteintrag gespeichert oder verworfen werden soll
        QMessageBox msgBox;
        msgBox.setText(MAINWINDOW_MESSAGE_FINISH_TIME_RECORDING);
        msgBox.setInformativeText(MAINWINDOW_QUESTION_FINISH_TIME_RECORDING);
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();
        // Auswerten der Benutzereingabe
        switch (ret) {
        case QMessageBox::Save:
            prepareTimeEntryForDisplay();
            break;
        case QMessageBox::Discard:
            // Don't Save was clicked
            break;
        default:
            // should never be reached
            break;
        }
        // Zurücksetzen der UI Elemente
        lblTimer->setText("");
        ui->cmbIssues->setEnabled(true);
        ui->cmbProjects->setEnabled(true);
        ui->actionRecordTime->setText("Start");
    }
}

void MainWindow::cmbProjectsSelectedIndexChanged(int idx){
    ui->cmbIssues->clear();
    ui->lblDescription->setText("");
    ui->lblSubject->setText("");
    ui->cmbIssues->setEnabled(false);
    if(ui->cmbProjects->count() > 0){
        RedmineProject *project = ui->cmbProjects->itemData(idx).value<RedmineProject*>();
        progress->show();
        redmineManager->getIssues(project->identifier);
    }
}

void MainWindow::cmbIssuesSelectedIndexChanged(int idx){

    RedmineIssue *issue = ui->cmbIssues->itemData(idx).value<RedmineIssue*>();
    if(issue){
        ui->lblSubject->setText("["+ issue->tracker +"] " + issue->subject);
        ui->lblAssignedTo->setText("Assigned to: " +issue->assignedTo);
        ui->lblStatus->setText("Status: " + issue->status);
        ui->lblDescription->setText(issue->description);
        ui->cmbIssues->setCurrentText("#" + QString::number(issue->id));
        ui->actionRecordTime->setEnabled(true);
        ui->cmbIssues->setEnabled(true);
    }
    else{
        ui->actionRecordTime->setEnabled(false);
    }
}

/*-------------------------------------------------------------------------------
 * Methoden
--------------------------------------------------------------------------------*/

void MainWindow::prepareTimeEntryForDisplay() {
    prepareTimeEntryForDisplay(currentTimeRecord);
}

void MainWindow::prepareTimeEntryForDisplay(RedmineTimeEntry entryItem) {
    // erstellen eines neuen Zeiteintrags
    TimeEntryItem *record = new TimeEntryItem(entryItem);
    // verbinden zur methode die das event händelt
    connect(record,SIGNAL(uploadSuccessfull(TimeEntryItem*)),this,SLOT(timeEntryUploadSuccessfull(TimeEntryItem*)));
    QListWidgetItem *item = new QListWidgetItem();
    // erstellen des UI ELements um den Zeiteintrag darzustellen
    item->setSizeHint(QSize(0,160));
    ui->lstTimeEntryList->addItem(item);
    ui->lstTimeEntryList->setItemWidget(item, record);
}

void MainWindow::connectSlots(){
    // Verbinden der Events auf die jeweiligen Mehtoden
    connect(ui->actionSettings, SIGNAL(triggered()),this, SLOT(btnSettingsClicked()));
    connect(ui->actionRecordTime, SIGNAL(toggled(bool)),this, SLOT(btnRecordTimeClicked(bool)));
    connect(ui->actionRefreshProjects, SIGNAL(triggered()),this, SLOT(btnRefreshClicked()));
    connect(ui->cmbProjects,SIGNAL(currentIndexChanged(int)),this,SLOT(cmbProjectsSelectedIndexChanged(int)));
    connect(ui->cmbIssues,SIGNAL(currentIndexChanged(int)),this,SLOT(cmbIssuesSelectedIndexChanged(int)));
    connect(trackerController,SIGNAL(update(QString)),this,SLOT(updateTimer(QString)));
}

void MainWindow::loadUnpublishedEntries() {
    // Läd die gespeicherten Einträge vom lokalen Dateisystem
    QList<RedmineTimeEntry> entries = TimeEntryItem::deserialize();
    // Fügt die Elemente einzeln zu Liste der UI hinzu
    foreach (RedmineTimeEntry entry, entries) {
        prepareTimeEntryForDisplay(entry);
    }
}

void MainWindow::setupCustomUI() {
    // Spezielle OSX UI Anpassung womit die Toolbar und die Titelleiste verschmelzen
    setUnifiedTitleAndToolBarOnMac(true);

    // Erstellen einer eigenen Palette um bestimmte Kontrollelemente einzufärben
    QPalette p = ui->lblDescription->palette();
    p.setColor(QPalette::Base, QColor(0,0,0,0)); // r,g,b,A
    ui->lblDescription->setPalette(p);
    ui->lstTimeEntryList->setPalette(p);

    // Erstellen des Labels zur Anzeige der ablaufenden Zeit
    // wenn der Aufnahme Modus aktiviert ist
    lblTimer = new QLabel("",this);
    QFont font = lblTimer->font();
    font.setFamily("Roboto");
    font.setPointSize(30);

    // Fixt ein Datestellungsproblem der Schrift unter OSX
#ifdef Q_OS_MACX
    font.setStretch(75);
#endif
    // Deaktiviert den Fokus Rahmen für das ListElement
    ui->lstTimeEntryList->setAttribute(Qt::WA_MacShowFocusRect, false);
    // Erstellt und Konfiguriert Die Progressbar zum anzeigen der aktivität der Netzwerkkomunikation
    this->progress = new QProgressBar(this);
    progress->setMaximum(0);
    progress->setMinimum(0);
    progress->hide();
    progress->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    ui->statusBar->showFullScreen();

    // Erstellt Einen Abstandshalter um die Progressbar mittig zu positionieren
    QWidget* statusBar_spacer = new QWidget();
    statusBar_spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    // Hinzufügen des Abstandshalters und der Progressbar
    ui->statusBar->addWidget(statusBar_spacer,0);
    ui->statusBar->addWidget(progress,0);
    // Anpassung der Schriftart
    lblTimer->setFont(font);
    lblTimer->setStyleSheet("margin-right:10px");
    // Abstandshalter um das Label für die Zeitanzeige Rechtsbündig auszurichten
    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    // Abstandshalter für den Reichten Rand
    QWidget* spacerEnd = new QWidget();
    spacerEnd->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    spacerEnd->setMinimumWidth(10);
    // Hinzufügen des Labels und der beiden Abstandshalter
    ui->mainToolBar->insertWidget(ui->actionRecordTime,spacer);
    ui->mainToolBar->insertWidget(ui->actionRecordTime,lblTimer);
    ui->mainToolBar->addWidget(spacerEnd);
}

void MainWindow::reloadRedmineData(){
    // Anzeigen der Progressbar sobald ein neueladen der Daten vom Server angestoßen wurde
    progress->show();
    // Überprüfung welche Authentifizierungsmehtode genutzt wurde
    // mit nachfolgender Initalisierung des Redmine Clients
    if(SettingsManager::instance()->redmineUseApiKeyAuthMethod){
        QUrl url =  QUrl(SettingsManager::instance()->redmineServerAddress);
        redmineManager = new RedmineClient(url,SettingsManager::instance()->redmineAPIKey);
    }else{
        redmineManager = new RedmineClient(SettingsManager::instance()->redmineAccountData());
    }

    /*-------------------------------------------------------------------------------
     * Connect REST Client Response Slots
    --------------------------------------------------------------------------------*/
    connect(redmineManager,SIGNAL(fetchRedmineProjectsCompleted(QList<RedmineProject*>)),this,SLOT(fetchProjectCompleted(QList<RedmineProject*>)));
    connect(redmineManager,SIGNAL(fetchProjectIssuesCompleted(QMap<int,RedmineIssue*>)),this,SLOT(fetchIssuesCompleted(QMap<int,RedmineIssue*>)));
    connect(redmineManager,SIGNAL(apiErrorOccurs(QString)),this,SLOT(handleError(QString)));
    // Aufruf an den Server um alle Projekte abzurufen
    redmineManager->getProjects();
}

void MainWindow::updateTimer(QString timeString) {
    // Aktualisiert die Zeitanzeige bei laufenden Timer
    this->lblTimer->setText(timeString);
}

void MainWindow::closeEvent(QCloseEvent *event) {
    // Wenn die Anwendung geschlossen werden soll wird geprüft ob es unveröffentlichte Zeiteinträge gibt
    // Wenn es welche gibt wir eine Dialog geöffnet in dem der Nutzer gefragt wird ob die noch nicht
    // veröffentlichten Daten gespeichert werden sollen?
    if (ui->lstTimeEntryList->count() > 0) {
        // Anzeigen des Dialogs
        QMessageBox::StandardButton resBtn = QMessageBox::question( this, "TrackIt!",
                                                                    tr("Not all entries are transferred to the server!\nShould the records be stored?"),
                                                                    QMessageBox::No | QMessageBox::Yes,
                                                                    QMessageBox::Yes);

        // Prüfen der Nutzereingabe
        // Wenn Ja gedrückt wurde dann werden alle einträge auf dem lokalen Dateisystem gespeichert
        if (resBtn == QMessageBox::Yes) {
            for(int i = 0; i < ui->lstTimeEntryList->count(); ++i)
            {
                QListWidgetItem* item = ui->lstTimeEntryList->item(i);

                TimeEntryItem *entry = (TimeEntryItem*)ui->lstTimeEntryList->itemWidget(item);
                entry->serialize();
            }
        }
    }
    event->accept();
}

/*-------------------------------------------------------------------------------
 * REST Client Response Slots
--------------------------------------------------------------------------------*/

void MainWindow::fetchProjectCompleted(QList<RedmineProject*> projects){
    //löscht alle eintrage in der combobox bevor neue hinzugefügt werden
    ui->cmbProjects->clear();
    //fügt jedes einzelne Projekt zur combobox hinzu und stellt sie hierarchisch dargestellt
    foreach (RedmineProject *project, projects) {
        int parents = 0;
        RedmineProject *temp = project;
        while (temp != NULL && temp->parentProject != NULL) {
            parents++;
            temp = temp->parentProject;
        }

        QString itemName = project->name;
        for (int i=parents; i>=0; i--) {
            if (i == parents-1) {
                itemName = "  ▸" + itemName;
            } else {
                itemName = "  " + itemName;
            }
        }
        ui->cmbProjects->addItem(itemName,QVariant::fromValue(project));
    }
    //wenn projekte hinzugefügt wurden dann selektiere das erste element indem man den index auf 0 setzt
    if(projects.size() > 0){
        ui->cmbProjects->setCurrentIndex(0);
    }
}

void MainWindow::fetchIssuesCompleted(QMap<int,RedmineIssue*> issues){
    // Nach dem alle Tickets abgerufen wurden werden sie der Combobox hinzugefügt
    foreach (RedmineIssue *issue, issues) {
        ui->cmbIssues->addItem("#"+ QString::number(issue->id) + " - " + issue->subject,QVariant::fromValue(issue));
    }
    progress->hide();
}

void MainWindow::handleError(QString message) {
    // Da ein Fehler aufgetreten ist wird die Fortschrittsleiste ausgeblendet
    progress->hide();
    // Anzeigen der Fehlermeldung
    QMessageBox::critical(this, "TrackIt",message,QMessageBox::Ok,QMessageBox::Ok);
}

void MainWindow::timeEntryUploadSuccessfull(TimeEntryItem *entry){

    //Sucht den Hochgeladenen Eintrag in der Liste und löscht ihn heraus
    for(int i = 0; i < ui->lstTimeEntryList->count(); ++i)
    {
        QListWidgetItem* item = ui->lstTimeEntryList->item(i);
        TimeEntryItem *entryForCompare = (TimeEntryItem*)ui->lstTimeEntryList->itemWidget(item);
        if (entryForCompare == entry) {
            // löschen des Eintrags aus der Liste
            ui->lstTimeEntryList->takeItem(i);
        }
    }
}








