#include "ViewController/mainwindow.h"
#include <QApplication>
#include <QFontDatabase>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // Hinzufügen der Customfont
    QFontDatabase::addApplicationFont(":/fonts/Fonts/Roboto-Thin.ttf");
    QFontDatabase::addApplicationFont(":/fonts/Fonts/Roboto-Bold.ttf");
    // Anpassen der globalen Linkdarstellung
    QPalette newPal(qApp->palette());
    newPal.setColor(QPalette::Link, QColor(23,85,169,255));
    newPal.setColor(QPalette::LinkVisited, QColor(23,85,169,255));
    qApp->setPalette(newPal);

    MainWindow w;
    w.show();
    return a.exec();
}
